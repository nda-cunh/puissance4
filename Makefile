NAME=SDL_main.elf
SRC= main.vala grille.vala burger.vala jeton.vala
LIB= --pkg=sdl2
VFLAGS=--Xcc=-O3
all:
	valac $(SRC) $(LIB) -o $(NAME) -g #$(VFLAGS)
toC:
	valac $(SRC) $(LIB) -C
install:
	echo "null"
mrproper:
	rm -rf *.c
	rm -rf $(NAME)
debug:
	valac $(SRC) $(LIB) -C
	gcc main.c window.c `pkg-config --cflags sdl2`
run:
	./$(NAME)
