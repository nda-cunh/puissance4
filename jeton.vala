//valac --pkg sdl2 --pkg SDL2_gfx main.vala burger.vala grille.vala
// 
class Jeton{
	public Jeton (ref Texture tex, Vector2i position, E_COLOR color) {
		m_sprite = new Sprite(tex);
		m_sprite.set_position(position);
		m_is_exist = false;
		m_color = color;
	}
	public void draw(ref Window win)
	{
		win.draw(m_sprite);
	}
	public Vector2i get_position(){
		return m_sprite.get_position();
	}
	public bool exist(){
		return m_is_exist;
	}
	public E_COLOR get_color(){
		return m_color;
	}
	public void set_exist(bool exist)
	{
		m_is_exist = exist;
	}
	private E_COLOR m_color;
	private Sprite m_sprite;
	private bool m_is_exist;
}

enum E_COLOR{
	RED,JAUNE
}
