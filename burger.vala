//valac --pkg sdl2 --pkg SDL2_gfx main.vala grille.vala burger.vala
const int POS_CENTER = (int)SDL.Video.Window.POS_CENTERED;

class Window{
	public Window(string title = "hello", int size_x = 500, int size_y = 500) {
		print("creation de la fenetre\n");
		m_title = title;
		m_size_x = size_x;
		m_size_y = size_y;
		m_is_open = true;
		m_win = new SDL.Video.Window(m_title, POS_CENTER, POS_CENTER, m_size_x, m_size_y, SDL.Video.WindowFlags.SHOWN);
		m_render = SDL.Video.Renderer.create (m_win, -1, SDL.Video.RendererFlags.SOFTWARE);
	}
	public bool is_open(){
		return m_is_open;            
	}
	public void draw(Sprite sprite){
		sprite.set_render(m_render);
		Texture text_tmp = sprite.get_surface();
		m_render.copy(sprite.get_texture(), {0,0,text_tmp.get_W(), text_tmp.get_H()}, {sprite.get_position().x,sprite.get_position().y, text_tmp.get_W() * sprite.get_scale().x ,text_tmp.get_H() * sprite.get_scale().y}); 
	}

	public void clear(){
		m_render.clear ();
		m_render.set_draw_color (255, 255, 255, 250);
		m_render.fill_rect ( {0, 0, 1024, 768} ) ;
	}
	public void close(){
		m_win.destroy();
		m_is_open = false;
	}
	public void present(){
		m_render.present();
		m_win.update_surface();
	}
	public SDL.Video.Renderer *get_render(){
		return m_render;
	}

	private bool m_is_open;
	private string m_title;
	private int m_size_x;
	private int m_size_y;
	private SDL.Video.Window m_win;
	private SDL.Video.Renderer m_render;
}

struct Vector2i{
	int x;
	int y;
}


class Texture{
	public Texture(string bpm){
		m_surface = new SDL.Video.Surface.from_bmp(bpm);
		assert(m_surface != null);
	}
	public int get_W(){
		return m_surface.w;
	}
	public int get_H(){
		return m_surface.h;
	}
	public SDL.Video.Surface *get_image(){
		return m_surface;
	}

	private SDL.Video.Surface m_surface;
}



class Sprite{
	public Sprite(Texture texture){
		m_texture = texture;
		m_size.x = 1;
		m_size.y = 1;
	}
	public void set_render(SDL.Video.Renderer render){
		m_texture_sdl = SDL.Video.Texture.create_from_surface (render, m_texture.get_image());
	}
	public SDL.Video.Texture *get_texture(){
		return m_texture_sdl;
	}
	public Texture *get_surface(){
		return m_texture;
	}
	public void scale(Vector2i size){
		m_size.x += size.x;
		m_size.y += size.y;
	}
	public void set_scale(Vector2i size){
		m_size.x = size.x;
		m_size.y = size.y;
	}
	public void set_position(Vector2i pos){
		m_position.x = pos.x;
		m_position.y = pos.y;
	}
	public void move(Vector2i position){
		m_position.x = m_position.x + position.x;
		m_position.y = m_position.y + position.y;
	}
	public Vector2i get_scale(){
		return m_size;
	}
	public Vector2i get_position(){
		return m_position;
	}
	private Vector2i m_size;
	private Vector2i m_position;
	private SDL.Video.Texture m_texture_sdl;
	private Texture m_texture;
}
